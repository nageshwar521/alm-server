const http = require('http');

module.exports.getJson = (options, onResult) => {
  // console.log('rest::getJSON');
  let output = '';

  const req = http.request(options, (res) => {
    // console.log(`${options.host} : ${res.statusCode}`);
    res.setEncoding('utf8');

    res.on('data', (chunk) => {
      output += chunk;
    });

    res.on('end', () => {
      let obj = JSON.parse(output);

      onResult(res.statusCode, obj);
    });
  });

  req.on('error', (err) => {
    res.send('error: ' + err.message);
  });

  req.end();
};