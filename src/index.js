import cors from "cors";
import express from "express";
import { getJson } from "./getJson";
const cool = require('cool-ascii-faces');

const app = express();
const port = process.env.PORT || 4000;

app.use(cors());

app.get("/api", (req, res) => {
  const options = {
    host: "hotels-gateway.tajawal.com",
    path: `/api/enigma/autocomplete/?query=${req.query.query}`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      token: "s732873t$28jKgbGnmMEU83"
    }
  };
  // console.log(options);
  getJson(options, (statusCode, result) => {
    // console.log(`onResult: (${statusCode})\n\n${JSON.stringify(result)}`);

    res.statusCode = statusCode;

    res.send(result);
  });
});

app.get('/cool', (req, res) => res.send(cool()));

app.listen(port, () => console.log(`Node app is running on port ${port}`));
